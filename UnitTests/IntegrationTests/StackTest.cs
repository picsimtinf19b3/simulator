﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using PicSimulator.Simulator;

namespace UnitTests.IntegrationTests
{
    [TestClass]
    public class StackTest
    {
        [TestMethod]
        public void TestPushLessThanBufferSizePopSame()
        {
            //Arange
            var stack = new PicStack();
            //Act
            for (UInt16 i = 1; i <= PicStack.BufferLength - 1; i++)
                stack.push(i);
            //Assert
            for (UInt16 i = PicStack.BufferLength - 1; i >= 1; i--)
                Assert.AreEqual(i, stack.pop(), stack.ToString());
        }

        [TestMethod]
        public void TestPushEqualThanBufferSizeAndPopSame()
        {
            //Arrange
            var stack = new PicStack();
            //Act
            for(UInt16 i = 1; i <= PicStack.BufferLength; i++)
                stack.push(i);
            //Assert
            for(UInt16 i = PicStack.BufferLength; i >= 1; i--)
                Assert.AreEqual(i, stack.pop(), stack.ToString());
        }

        [TestMethod]
        public void TestPushMoreThanBufferLength()
        {
            //Arrange
            //fill buffer with 1-8
            var stack = new PicStack();
            //Act
            for(UInt16 i = 1; i <= PicStack.BufferLength; i++)
                stack.push(i);
            //push one more addres, due to the ringbuffer functionality we now expect
            //the value 1 to be lost and thus should pop 9,8,7,6,5,4,3,2
            UInt16 pushVal = PicStack.BufferLength + 1;
            stack.push(pushVal);
            //Assert
            for(UInt16 i = pushVal; i >= 2; i--) {
                Assert.AreEqual(i, stack.pop(), stack.ToString());
            }
        }

        [TestMethod]
        public void TestMax13BitValue()
        {
            //Arrange
            var stack = new PicStack();
            //Act
            stack.push(0x1FFF);
            //Assert
            Assert.AreEqual(0x1FFF, stack.pop(), stack.ToString());
        }

        [TestMethod]
        public void TestMoreThan13BitMaskedOut()
        {
            //Arrange
            var stack = new PicStack();
            //Act
            stack.push(0x2000);
            //Assert
            Assert.AreEqual(0x00, stack.pop(), stack.ToString());

        }
    }
}