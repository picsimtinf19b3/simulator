﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PicSimulator._2ApplicationLayer;
using PicSimulator.Simulator;

namespace UnitTests.IntegrationTests.InstructionTests.ByteOriented
{
    [TestClass]
    public class MovlwTest
    {
        [TestMethod]
        public void TestMovlw()
        {
            //Arrange
            UInt16 OpCodeMOVLW = 0x3011;
            var picStack = new PicStack();
            var pclMediator = new PCLMediator();
            var picMemory = new Memory(pclMediator);
            var picProgramCounter = new ProgramCounter(pclMediator);
            var picProgramMemory = new ProgramMemory();
            var picInstructionDecoder = new InstructionDecoder();
            var pic = new Pic16F84(picStack, picMemory, picProgramCounter,
                picProgramMemory, picInstructionDecoder);
            var instruction = new InstructionDecoder().decode(OpCodeMOVLW);
            //Act
            instruction.execute(pic);
            //Assert
            Assert.AreEqual(0x11, pic.WRegister);
        }
    }
}
