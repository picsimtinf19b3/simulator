﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PicSimulator._1DomainLayer;
using PicSimulator._2ApplicationLayer;
using PicSimulator.Simulator;
using PicSimulator.Simulator.Instruction;

namespace UnitTests.IntegrationTests.InstructionTests.LiteralAndControl
{
    [TestClass]
    public class SUBLWTest
    {
        [TestMethod]
        public void TestNoOverflowsAndNotZero()
        {
            //Arrange
            var picStack = new PicStack();
            var pclMediator = new PCLMediator();
            var picMemory = new Memory(pclMediator);
            var picProgramCounter = new ProgramCounter(pclMediator);
            var picProgramMemory = new ProgramMemory();
            var picInstructionDecoder = new InstructionDecoder();
            var pic = new Pic16F84(picStack, picMemory, picProgramCounter,
                picProgramMemory, picInstructionDecoder)
            {
                WRegister = 1
            };
            UInt16 opCodeSUBLW = 0x3C02;
            var instruction = new InstructionDecoder().decode(opCodeSUBLW);
            //Act
            instruction.execute(pic);
            //Assert
            Assert.AreEqual(false, pic.Memory.getZFlag(), "ZFlag");
            Assert.AreEqual(false, pic.Memory.getCFlag(), "CFlag");
            Assert.AreEqual(false, pic.Memory.getDCFlag(), "DCFlag");
        }

        [TestMethod]
        public void TestZeroFlagNoOverflows()
        {
            //Arrange
            var picStack = new PicStack();
            var pclMediator = new PCLMediator();
            var picMemory = new Memory(pclMediator);
            var picProgramCounter = new ProgramCounter(pclMediator);
            var picProgramMemory = new ProgramMemory();
            var picInstructionDecoder = new InstructionDecoder();
            var pic = new Pic16F84(picStack, picMemory, picProgramCounter,
                picProgramMemory, picInstructionDecoder)
            {
                WRegister = 2
            };
            UInt16 opCodeSUBLW = 0x3C02;
            var instruction = new InstructionDecoder().decode(opCodeSUBLW);
            //Act
            instruction.execute(pic);
            //Assert
            Assert.AreEqual(true, pic.Memory.getZFlag(), "ZFlag");
            Assert.AreEqual(false, pic.Memory.getCFlag(), "CFlag");
            Assert.AreEqual(false, pic.Memory.getDCFlag(), "DCFlag");
        }

        [TestMethod]
        public void TestDcFlag()
        {
            //Arrange
            var picStack = new PicStack();
            var pclMediator = new PCLMediator();
            var picMemory = new Memory(pclMediator);
            var picProgramCounter = new ProgramCounter(pclMediator);
            var picProgramMemory = new ProgramMemory();
            var picInstructionDecoder = new InstructionDecoder();
            var pic = new Pic16F84(picStack, picMemory, picProgramCounter,
                picProgramMemory, picInstructionDecoder)
            {
                WRegister = 1
            };
            UInt16 opCodeSUBLW = 0x3C10;
            var instruction = new InstructionDecoder().decode(opCodeSUBLW);
            //Act
            instruction.execute(pic);
            //Assert
            Assert.AreEqual(false, pic.Memory.getZFlag(), "ZFlag");
            Assert.AreEqual(false, pic.Memory.getCFlag(), "CFlag");
            Assert.AreEqual(true, pic.Memory.getDCFlag(), "DCFlag");
        }

        [TestMethod]
        public void TestCFlag()
        {
            //Arrange
            var picStack = new PicStack();
            var pclMediator = new PCLMediator();
            var picMemory = new Memory(pclMediator);
            var picProgramCounter = new ProgramCounter(pclMediator);
            var picProgramMemory = new ProgramMemory();
            var picInstructionDecoder = new InstructionDecoder();
            var pic = new Pic16F84(picStack, picMemory, picProgramCounter,
                picProgramMemory, picInstructionDecoder)
            {
                WRegister = 3
            };
            UInt16 opCodeSUBLW = 0x3C02;
            var instruction = new InstructionDecoder().decode(opCodeSUBLW);
            //Act
            instruction.execute(pic);
            //Assert
            Assert.AreEqual(true, pic.Memory.getDCFlag(), "DCFlag");
            Assert.AreEqual(true, pic.Memory.getCFlag(), "CFlag");
            Assert.AreEqual(false, pic.Memory.getZFlag(), "ZFlag");
        }
    }
}
