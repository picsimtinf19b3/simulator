﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PicSimulator.Simulator;
using PicSimulator.Simulator.Instruction.ByteOriented;

namespace UnitTests.IntegrationTests
{
    [TestClass]
    public class InstructionDecoderTest
    {
        [TestMethod]
        public void TestDecode()
        {
            //Arange
            UInt16 opCodeMOVF = 0x0800;
            var instructionDecoder = new InstructionDecoder();
            //Act
            var instruction = instructionDecoder.decode(opCodeMOVF);
            //Assert
            Assert.IsInstanceOfType(instruction, typeof(MOVF));
        }
    }
}
