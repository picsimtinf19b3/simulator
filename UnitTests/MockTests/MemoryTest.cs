﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PicSimulator._2ApplicationLayer;
using PicSimulator.Simulator;

namespace UnitTests.MockTests
{
    [TestClass]
    public class MemoryTest
    {
        [TestMethod]
        public void TestSetByte()
        {
            //Arrange
            var mediator = new Mock<IPCLMediator>();
            var mem = new Memory(mediator.Object);
            //Act
            mem.setByte(0x20, 42);
            //Assert
            Assert.AreEqual(42, mem.getByte(0x20), ("Value: " + mem.getByte(0x20)));
        }

        [TestMethod]
        public void TestGetByte() {
            //Arrange
            var mediator = new Mock<IPCLMediator>();
            var mem = new Memory(mediator.Object);
            //Act
            mem.setBit(0x21, 0, true); 
            mem.setBit(0x21, 1, false);
            mem.setBit(0x21, 2, false);
            mem.setBit(0x21, 3, true);
            //Assert
            Assert.AreEqual(true, mem.getBit(0x21, 0));
            Assert.AreEqual(false, mem.getBit(0x21, 1));
            Assert.AreEqual(false, mem.getBit(0x21, 2));
            Assert.AreEqual(true, mem.getBit(0x21, 3));
        }
        
        [TestMethod]
        public void TestSetSpecialByte()
        {
            //Arrange
            var mediator = new Mock<IPCLMediator>();
            var mem = new Memory(mediator.Object);
            //Act
            mem.setByte((UInt16) RegistersBank0.FSR, 42);
            //Assert
            Assert.AreEqual(42, mem.getByte((UInt16)RegistersBank0.FSR), ("Value bank 0: " + mem.getByte((UInt16)RegistersBank0.FSR)));
            Assert.AreEqual(42, mem.getByte((UInt16)RegistersBank1.FSR), ("Value bank 1: " + mem.getByte((UInt16)RegistersBank1.FSR)));
        }

        [TestMethod]
        public void TestSetSpecialBit()
        {
            //Arrange
            var mediator = new Mock<IPCLMediator>();
            var mem = new Memory(mediator.Object);
            //Act
            mem.setBit((UInt16)RegistersBank1.FSR, 7, true);
            //Assert
            Assert.AreEqual(0x80, mem.getByte((UInt16)RegistersBank0.FSR), ("Value bank 0: " + mem.getByte((UInt16)RegistersBank0.FSR)));
            Assert.AreEqual(0x80, mem.getByte((UInt16)RegistersBank1.FSR), ("Value bank 1: " + mem.getByte((UInt16)RegistersBank1.FSR)));
        }

        [TestMethod]
        public void TestMemBank0()
        {
            //Arrange
            var mediator = new Mock<IPCLMediator>();
            var mem = new Memory(mediator.Object);
            //Act
            mem.setByte(0x20, 24);
            //Assert
            Assert.AreEqual(24, mem.getByte(0x20));
            mem.setBit((UInt16)RegistersBank0.STATUS, 5, true);
            Assert.AreEqual(0, mem.getByte(0x20));

        }

        [TestMethod]
        public void TestMemBank1()
        {
            //Arrange
            var mediator = new Mock<IPCLMediator>();
            var mem = new Memory(mediator.Object);
            //Act
            mem.setBit((UInt16)RegistersBank0.STATUS, 5, true);
            mem.setByte(0x20, 42);
            //Assert
            mem.setBit((UInt16)RegistersBank0.STATUS, 5, false);
            Assert.AreEqual(0, mem.getByte(0x20));
            mem.setBit((UInt16)RegistersBank0.STATUS, 5, true);
            Assert.AreEqual(42, mem.getByte(0x20));
        }

        [TestMethod]
        public void TestPC()
        {
            //Arrange
            var mediator = new Mock<PCLMediator>();
            var mem = new Memory(mediator.Object);
            var pc = new ProgramCounter(mediator.Object);
            //Act
            pc.Increment();
            pc.Increment();
            //Assert
            Assert.AreEqual(2, mem.getByte((UInt16)RegistersBank0.PCL));
            Assert.AreEqual(2, mem.getByte((UInt16)RegistersBank1.PCL));
        }

        [TestMethod]
        public void TestSetPc8()
        {
            //Arrange
            var mediator = new Mock<PCLMediator>();
            var mem = new Memory(mediator.Object);
            var pc = new ProgramCounter(mediator.Object);
            //Act
            pc.SetPc8(0x20);
            //Assert
            Assert.AreEqual(0x20, mem.getByte((UInt16)RegistersBank0.PCL));
            Assert.AreEqual(0x20, mem.getByte((UInt16)RegistersBank1.PCL));
        }

        [TestMethod]
        public void TestSetPc11()
        {
            //Arrange
            var mediator = new Mock<PCLMediator>();
            var mem = new Memory(mediator.Object);
            var pc = new ProgramCounter(mediator.Object);
            //Act
            pc.SetPc11(0x40);
            //Assert
            Assert.AreEqual(0x40, mem.getByte((UInt16)RegistersBank0.PCL));
            Assert.AreEqual(0x40, mem.getByte((UInt16)RegistersBank1.PCL));
        }
    }
}