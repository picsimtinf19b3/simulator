﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PicSimulator.Simulator;
using PicSimulator._2ApplicationLayer;

namespace UnitTests.MockTests
{
    [TestClass]
    public class ProgramCounterTest
    {
        [TestMethod]
        public void TestIncrement()
        {
            //Arrange
            var pclMediator = new Mock<IPCLMediator>();
            var pc = new ProgramCounter(pclMediator.Object);
            //Act
            pc.Increment();
            //Assert
            Assert.AreEqual( 0x01, pc.ProgramCounterValue, ("PC: " + pc.ProgramCounterValue));
        }

        [TestMethod]
        public void TestPCIncTurnaround()
        {
            //Arrange
            var pclMediator = new Mock<IPCLMediator>();
            var pc = new ProgramCounter(pclMediator.Object);
            //Act
            for (int i = 0; i < 0x2000; i++)
            {
                pc.Increment();
            }
            //Assert
            Assert.AreEqual(0x00, pc.ProgramCounterValue,("PC: " + pc.ProgramCounterValue));
        }


        [TestMethod]
        public void TestPCLath8()
        {
            //Arrange
            var pclMediator = new Mock<IPCLMediator>();
            var pc = new ProgramCounter(pclMediator.Object);
            //Act
            pc.SetPcLath(0x11);
            pc.SetPc8(0xFF);
            //Assert
            Assert.AreEqual(0x11FF, pc.ProgramCounterValue, ("PC: " + pc.ProgramCounterValue));
        }


        [TestMethod]
        public void TestPCLath11()
        {
            //Arrange
            var pclMediator = new Mock<IPCLMediator>();
            var pc = new ProgramCounter(pclMediator.Object);
            //Act
            pc.SetPcLath(0x1D);
            pc.SetPc11(0x2FF);
            //Assert
            Assert.AreEqual(0x1AFF, pc.ProgramCounterValue, ("PC: " + pc.ProgramCounterValue));
        }
    }
}
