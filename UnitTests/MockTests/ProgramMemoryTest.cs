﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PicSimulator._1DomainLayer;
using PicSimulator.Simulator;

namespace UnitTests.MockTests
{
    [TestClass]
    public class ProgramMemoryTest
    {
        [TestMethod]
        public void TestLoadProgram()
        {
            //Arrange
            var lines = new List<ProgramLine>{new ProgramLine(0000, 3011)};
            var programLoader = new Mock<IProgramLoader>();
            programLoader.Setup(x => x.loadProgram()).Returns(lines);
            var programMemory = new ProgramMemory();
            //Act
            programMemory.loadProgram(programLoader.Object);
            //Assert
            Assert.AreEqual(programMemory.getOpCode(0000), 3011);

        }
    }
}
