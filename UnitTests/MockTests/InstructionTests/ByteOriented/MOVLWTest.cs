﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PicSimulator;
using PicSimulator._1DomainLayer;
using PicSimulator.Simulator;
using System;

namespace UnitTests.MockTests.InstructionTests.ByteOriented
{
    [TestClass]
    public class MOVLWTest
    {
        [TestMethod]
        public void TestExecute()
        {
            //Arrange
            UInt16 OpCodeMOVLW = 0x3011;
            var instructionDecoder = new Mock<IInstructionDecoder>();
            var programMemory = new Mock<IProgramMemory>();
            var ram = new Mock<IMemory>();
            var stack = new Mock<IPicStack>();
            var programCounter = new Mock<IProgramCounter>();
            var pic = new Pic16F84(stack.Object, ram.Object, programCounter.Object,
                programMemory.Object, instructionDecoder.Object);
            var instruction = new InstructionDecoder().decode(OpCodeMOVLW);
            //Act
            instruction.execute(pic);
            //Arrange
            Assert.AreEqual(0x11, pic.WRegister);
        }

    }
}
